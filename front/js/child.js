import {addChild, getChild, getTestFreeResult, getAllClubsByChildId} from "./API.js";

$(document).ready(function() {
    let userData = JSON.parse(localStorage.getItem("user"));
    let username = userData.username;

    const isValidateFirstname = (firstName) => {
        return String(firstName)
            .match(
                /^[A-Za-zА-Яа-яЁё]+$/
            );
    };

    const isValidateLastname = (lastname) => {
        return /^[A-Za-zА-Яа-яЁё]*$/.test(lastname);
    };

    const isValidateDigits = (input) => {
        return String(input).match(/^\d+$/);
    };

    function getChildData(){
        const urlParams = new URLSearchParams(window.location.search);
        const childId = urlParams.get('childId');
        getChild(childId)
            .then(function (response) {
                console.log(response);
                let childFirstname = response.firstname;
                let childLastname = response.lastname;
                let childAge = new Date().getFullYear() - response.birthYear;
                let childGender = response.gender;

                $('#childFirstName').text(childFirstname);
                $('#childLastName').text(childLastname);
                $('#childAge').text(childAge);
                $('#childGender').text(childGender);

                let linkToFreeTest = $('#linkToFreeTest');
                linkToFreeTest.append('<a href="#" onclick="redirectToFreeTest(' + childId + ')">Пройти бесплатный тест</a>');

                getTestFreeResult(childId)
                    .then(function(response) {
                        let freeTestInfo = $('#freeTestInfo');
                        let resultsList = $('#freeTestResultsList');

                        if (Array.isArray(response)) {
                            response.forEach(function(testFree) {
                                testFree.results.forEach(function(result) {
                                    resultsList.append('<li>' + result + '</li>');
                                });
                            });
                        } else if (response.results) {
                            linkToFreeTest.hide();
                            response.results.forEach(function(result) {
                                resultsList.append('<li>' + result + '</li>');
                            });
                        } else {
                            console.error('Invalid response format:', response);
                            return;
                        }
                        freeTestInfo.append('<h3>Результаты бесплатного тестирования:</h3>');
                        freeTestInfo.append(resultsList);
                    })
                    .catch(function(error) {
                        console.error('Error getting test results:', error);
                    });
                getAllClubsByChildId(childId)
                    .then(function(response) {
                        let childClubListInfo = $('#childClubListInfo');
                        let childClubList = $('#childClubList');

                        if (Array.isArray(response) && response.length > 0) {
                            response.forEach(function(item) {
                                childClubList.append('<li>Название клуба: <strong>' + item.name + '</strong></li>');
                            });
                            childClubListInfo.append('<h3>Список записанных клубов:</h3>');
                            childClubListInfo.append(childClubList);
                        }
                        else {
                            childClubListInfo.find('h3').hide();

                        }
                    })
                    .catch(function(error) {
                        console.error('Error getting clubs by child id:', error);
                    });
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                console.error("Error:", errorThrown);
            });
    }
    getChildData();

    const addChildContainer = $("#addChildContainer");
    const addChildLink = $("<a>").attr("href", "#").text("Добавить ребенка");
    addChildContainer.append(addChildLink);

    addChildLink.on("click", function(event) {
        event.preventDefault();
        const form = $("<form>");

        const firstnameInput = $("<input>").attr("type", "text").attr("placeholder", "Имя");
        const lastnameInput = $("<input>").attr("type", "text").attr("placeholder", "Фамилия");
        const genderSelect = $("<select>");
        $.each(["М", "Ж"], function(index, gender) {
            $("<option>").attr("value", gender).text(gender).appendTo(genderSelect);
        });
        const currentYear = new Date().getFullYear();
        const ageSelect = $("<select>");
        ageSelect.append($("<option>").text("Год рождения").attr("disabled", true).attr("selected", true));

        for (let year = 2006; year <= currentYear; year++) {
            const option = $("<option>").text(year);
            ageSelect.append(option);
        }
        const submitButton = $("<input>").attr("type", "submit").val("Добавить");

        form.append(firstnameInput, lastnameInput, genderSelect, ageSelect, submitButton);

        addChildContainer.empty().append(form);

        submitButton.on("click", async function(event) {
            event.preventDefault();
            let childFirstnameInputValue = firstnameInput.val();
            if(!isValidateFirstname(childFirstnameInputValue)){
                alert('Пожалуйста, введите корректное имя.');
                return;
            }

            let childLastnameInputValue = lastnameInput.val();
            if (!isValidateLastname(childLastnameInputValue)) {
                alert('Пожалуйста, введите корректную фамилию.');
                return;
            }
            const childBirthYearValue = ageSelect.val();
            if (!isValidateDigits(childBirthYearValue)) {
                alert('Пожалуйста, выберите корректный год рождения.');
                return;
            }
                const formData = {
                firstname: childFirstnameInputValue,
                lastname: childLastnameInputValue,
                gender: genderSelect.val(),
                birthYear: childBirthYearValue,
            };
            addChild(username, formData);
        });
    });

    window.redirectToFreeTest = redirectToFreeTest;
});

window.redirectToFreeTest =  function (childId){
    try {
        window.location.replace("test-free.html?childId=" + childId);
        event.preventDefault();
    } catch (error) {
        console.error('Error getting tests:', error);
    }
}

function downloadPdf() {
    const urlParams = new URLSearchParams(window.location.search);
    const childId = urlParams.get('childId');
    const token = JSON.parse(localStorage.getItem("user"))?.access_token;

    if (!childId) {
        console.error('Ошибка: не указан идентификатор ребенка');
        return;
    }

    const url = `http://localhost:8080/api/v1/export-pdf/${childId}`;
    const options = {
        method: "GET",
        headers: {
            'Authorization': 'Bearer ' + token
        }
    };

    fetch(url, options)
        .then(response => {
            if (!response.ok) {
                throw new Error('Сетевая ошибка: ответ сервера не успешен');
            }
            return response.blob();
        })
        .then(blob => {
            if (blob.size > 0) {
                const link = document.createElement('a');
                link.href = window.URL.createObjectURL(blob);
                link.download = 'child-info.pdf';
                link.click();
                window.URL.revokeObjectURL(link.href);
            } else {
                console.warn('Получен пустой файл');
            }
        })
        .catch(error => console.error('Ошибка:', error));
}

document.getElementById('downloadPdfButton').addEventListener('click', downloadPdf);
