const SERVER_URL_USERS= "http://localhost:8080/api/v1/users";
const SERVER_URL_LOGIN= "http://localhost:8080/api/v1/login";
const SERVER_URL_FREE_TEST= "http://localhost:8080/api/v1/free_test";
const SERVER_URL_CHILDREN= "http://localhost:8080/api/v1/children";
const SERVER_URL_ENROLLMENT= "http://localhost:8080/api/v1/enrollments";

export function getAllClubsByChildId(id) {
    return $.ajax({
        url: `${SERVER_URL_ENROLLMENT}/${id}`,
        method: "GET",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + JSON.parse(localStorage.getItem("user")).access_token);
        },
        xhrFields: {
            withCredentials: true
        },
        error: function (jqXHR, textStatus, error) {
            if (jqXHR.status === 401) {
                window.location.href = "login.html";
            }
            else {
                return null;
            }
        }
    });
}
export function addChild(username, formData) {
    return $.ajax({
        url: SERVER_URL_CHILDREN + '/' + username,
        method: "POST",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + JSON.parse(localStorage.getItem("user")).access_token);
        },
        contentType: 'application/json',
        data: JSON.stringify(formData),
        processData: false,

        success: function () {
            alert('Ребенок успешно зарегистрирован');
            window.location.href = "user-profile.html";
        },
        error: function (jqXHR, textStatus, error) {
            alert('Произошла ошибка при сохранении ребенка');
            console.error('Произошла ошибка:', error);
        }
    });
}

export function getChild(id) {
    return $.ajax({
        url: SERVER_URL_CHILDREN + '/' + id,
        method: "GET",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + JSON.parse(localStorage.getItem("user")).access_token);
        },
        xhrFields: {
            withCredentials: true
        },
        error: function (jqXHR, textStatus, error) {
            if (jqXHR.status === 401) {
                window.location.href = "login.html";
            }
        }
    });
}

export function getUser(username) {
    return $.ajax({
        url: SERVER_URL_USERS +'/' + username,
        method: "GET",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + JSON.parse(localStorage.getItem("user")).access_token);
        },
        xhrFields: {
            withCredentials: true
        },
        error: function (jqXHR, textStatus, error) {
            if (jqXHR.status === 401) {
                window.location.href = "login.html";
            }
        }
    });
}

export function passFreeTest(id, choiceIds) {
    return $.ajax({
        url: `${SERVER_URL_FREE_TEST}/${id}`,
        method: "POST",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + JSON.parse(localStorage.getItem("user")).access_token);
        },
        contentType: 'application/json',
        data: JSON.stringify(choiceIds),
        processData: false,

        success: function () {
            alert('Тест успешно пройден');
            window.location.href = "user-profile.html";
        },
        error: function (jqXHR, textStatus, error) {
            console.error('Произошла ошибка:', error);
        }
    });
}

export function getTestFreeResult(id) {
    return $.ajax({
        url: `${SERVER_URL_FREE_TEST}/${id}`,
        method: "GET",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + JSON.parse(localStorage.getItem("user")).access_token);
        },
        xhrFields: {
            withCredentials: true
        },
        error: function (jqXHR, textStatus, error) {
            if (jqXHR.status === 401) {
                window.location.href = "login.html";
            }
            else {
                //alert('Либо тест еще не пройден, либо не было совпадений в пройденном тесте');
                return null;
            }
        }
    });
}

export function getTest() {
    return $.ajax({
        url: SERVER_URL_FREE_TEST,
        method: "GET",
        dataType: "application/json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + JSON.parse(localStorage.getItem("user")).access_token);
        },
        success: function () {
            window.location.href = "test-free.html";
        },
        error: function (jqXHR, textStatus, error) {
            if (jqXHR.status === 401) {
                window.location.href = "index.html";
            }
        },
        xhrFields: {
            withCredentials: true
        }
    });
}

export function updateUserInfo(username, formData) {
    return $.ajax({
        url: `${SERVER_URL_USERS}/${username}`,
        method: "PUT",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + JSON.parse(localStorage.getItem("user")).access_token);
        },
        contentType: 'application/json',
        data: JSON.stringify(formData),
        processData: false,
        success: function () {
            alert('Данные успешно обновлены');
            window.location.href = "user-profile.html";
        },
        error: function (jqXHR, textStatus, error) {
            console.error('Произошла ошибка:', error);
        }
    });
}

export function postUser(formData){
    return $.ajax({
        url: SERVER_URL_USERS,
        method: "POST",
        contentType: 'application/json',
        data: JSON.stringify(formData),
        "processData": false,
        success: function (data) {
            alert('Пользователь успешно зарегистрирован');
            window.location.href = "login.html";
        },
        xhrFields: {
            withCredentials: true
        },
        error: function (jqXHR, textStatus, error) {
            if (jqXHR.status === 401) {
                window.location.href = "register.html";
            }
            else{
                console.log(error);
            }
        }
    });
}

export function loginUser(formData) {
    return $.ajax({
        url: SERVER_URL_LOGIN,
        method: "POST",
        data: formData,
        "processData": false,
        mimeType: "multipart/form-data",
        contentType: false,
        xhrFields: {
            withCredentials: true
        },
        success: function(data, textStatus, request) {
            var user = {
                username: formData.get('username'),
                access_token: request.getResponseHeader("access_token"),
                refresh_token: request.getResponseHeader("refresh_token"),
            };

            localStorage.setItem("user", JSON.stringify(user));

            if (user.username === 'admin@gmail.com') {
                window.location.href = "admin.html";
            } else {
                window.location.href = "user-profile.html";
            }
        },

        error: function (jqXHR, textStatus, error) {
            if (jqXHR.status === 401) {
                alert('Wrong credentials');
                window.location.href = "login.html";
            }
            else{
                console.log(error);
            }
        }
    });
}