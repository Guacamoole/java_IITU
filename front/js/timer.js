const apiUrl = "http://localhost:8080/api/v1";
const websocketUrl = apiUrl + "/stopwatch";
const queue = "/user/queue/test";
const app = "/app/startTimer";

const token = JSON.parse(localStorage.getItem("user")).access_token;
const headers = {
    Authorization: "Bearer " + token,
};

let client = null;

function handleMessage(message) {
    store.dispatch("setRestTime", message.body);
}

function connectWebSocket() {
    const sock = new SockJS(websocketUrl);
    client = Stomp.over(sock);

    const headers = {
        Authorization: "Bearer " + token,
        'Content-Type': 'text/event-stream'
    };
    client.connect(headers, () => {
        console.log("Connected");
        client.subscribe(queue, handleMessage);
        client.send(app, {}, JSON.stringify("Start"));
    });
}

export function init() {
    connectWebSocket();
}

window.addEventListener("load", init);

let restTime = null;

function updateRestTime(newState) {
    restTime = newState;
    document.getElementById("stopwatch").innerText = restTime;
}

const store = {
    state: {
        restTime: "",
    },
    dispatch: function (action, payload) {
        switch (action) {
            case "setRestTime":
                updateRestTime(payload);
                break;
            default:
                console.error("Unknown action:", action);
        }
    },
};

connectWebSocket();
