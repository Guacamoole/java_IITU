const SERVER_URL_CLUBS = "http://localhost:8080/api/v1/branches";
const SERVER_URL_CATEGORIES = "http://localhost:8080/api/v1/categories";
const SERVER_URL_SUBCATEGORIES = "http://localhost:8080/api/v1/subcategories";

document.addEventListener('DOMContentLoaded', async function () {
    result = await getAllClubs();
    await loadCategories();
    await loadSubcategories();
    await displayResult(result, rows, currentPage);
    displayPagination(result, rows);
});

async function makeRequest(url, params) {
    const token = JSON.parse(localStorage.getItem("user"))?.access_token;
    const fullUrl = url + "?" + new URLSearchParams(params).toString();

    try {
        const response = await fetch(fullUrl, {
            headers: token ? { 'Authorization': 'Bearer ' + token } : {},
        });

        if (!response.ok) {
            console.error(`Error ${response.status}: ${response.statusText}`);
            throw new Error(`Error ${response.status}: ${response.statusText}`);
        }

        return await response.json();
    } catch (error) {
        console.error(error);
    }
}

async function getClubById(clubId) {
    try {
        const urlWithId = `${SERVER_URL_CLUBS}/${clubId}`;
        return await makeRequest(urlWithId, {});
    } catch (error) {
        console.error("Ошибка при получении информации о клубе:", error);
        return null;
    }
}

async function getAllClubs() {
    try {
        return await makeRequest(SERVER_URL_CLUBS, {});
    } catch (error) {
        console.error("Error fetching clubs:", error);
        return null;
    }
}

async function loadCategories() {
    try {
        const categories = await makeRequest(SERVER_URL_CATEGORIES, {});

        const categoriesContainer = document.getElementById("categoriesContainer");
        categoriesContainer.innerHTML = "";

        categories.forEach(function (category) {
            const button = document.createElement("button");
            button.textContent = category.name;
            button.addEventListener("click", async function () {
                await filterByCategory(category.id);
                await loadSubcategories(category.id);
            });
            categoriesContainer.appendChild(button);
        });
    } catch (error) {
        console.error("Error loading categories:", error);
    }
}

async function filterByCategory(categoryId) {
    try {
        const result = await makeRequest(SERVER_URL_CLUBS + "/filterByCategory", { categoryId: categoryId });
        currentPage = 1;
        await displayResult(result, rows, currentPage);
        displayPagination(result, rows);
    } catch (error) {
        console.error("Error filtering by category:", error);
    }
}

async function loadSubcategories(categoryId) {
    try {
        const subcategories = await makeRequest(SERVER_URL_SUBCATEGORIES + `/by-category/${categoryId}`);

        const subcategoriesContainer = document.getElementById("subcategoriesContainer");
        subcategoriesContainer.innerHTML = "";

        subcategories.forEach(function (subcategory) {
            const button = document.createElement("button");
            button.textContent = subcategory.name;
            button.addEventListener("click", async function () {
                await filterBySubcategory(subcategory.id);
            });
            subcategoriesContainer.appendChild(button);
        });
    } catch (error) {
        console.error("Error loading subcategories:", error);
    }
}



async function filterBySubcategory(subcategoryId) {
    try {
        const result = await makeRequest(SERVER_URL_CLUBS + "/filterBySubcategory", { subcategoryId: subcategoryId });
        currentPage = 1;
        await displayResult(result, rows, currentPage);
        displayPagination(result, rows);
    } catch (error) {
        console.error("Error filtering by subcategory:", error);
    }
}

async function searchByName() {
    try {
        const name = document.getElementById("nameInput").value;
        const result = await makeRequest(SERVER_URL_CLUBS + "/searchByName", { name: name });
        currentPage = 1;
        await displayResult(result, rows, currentPage);
        displayPagination(result, rows);
    } catch (error) {
        console.error("Error searching by name:", error);
    }
}

let currentPage = 1;
let rows = 6;
let result;
let currentResult;

async function displayResult(result, rowPerPage, page) {
    const resultContainer = document.getElementById("resultContainer");
    resultContainer.innerHTML = " ";
    page--;

    currentResult = result;
    if (!result || !Array.isArray(result)) {
        console.error("Invalid result format:", result);
        return;
    }

    if (!result || result.length === 0) {
        resultContainer.innerHTML += "<p>No clubs found.</p>";
        return;
    }

    if (result.length === 0) {
        resultContainer.innerHTML += "<p>No clubs found.</p>";
    } else {
        const start = rowPerPage * page;
        const end = start + rowPerPage;
        const paginatedData = result.slice(start, end);

        paginatedData.forEach(function (club) {
            const clubCard = document.createElement('div');
            clubCard.classList.add('clubCard');

            const imgElement = document.createElement('img');
            imgElement.src = 'https://png.pngtree.com/png-vector/20190820/ourmid/pngtree-no-image-vector-illustration-isolated-png-image_1694547.jpg'
            clubCard.appendChild(imgElement);

            const nameElement = document.createElement('h3');
            nameElement.textContent = club.name;
            clubCard.appendChild(nameElement);

            const ratingOverlay = document.createElement('div');
            ratingOverlay.classList.add('rating-overlay');

            const ratingElement = document.createElement('p');
            ratingElement.classList.add('rating');
            const ratingStars = Array.from({ length: 5 }, (_, index) => {
                const star = document.createElement('span');
                star.classList.add('star');
                star.innerHTML = index < club.rating ? "&#9733;" : "&#9734;";
                return star.outerHTML;
            }).join('');

            ratingElement.innerHTML = `${ratingStars}`;
            ratingOverlay.appendChild(ratingElement);

            clubCard.appendChild(ratingOverlay);

            const infoElement = document.createElement('p');
            infoElement.innerHTML = `
                <strong>Адрес:</strong> ${club.address}<br>
                <strong>Телефонный номер:</strong> ${club.phoneNumber}<br>
                <strong>Инстаграм:</strong> <a href="${club.instagram}" target="_blank">${club.instagram}</a><br>
            `;
            clubCard.appendChild(infoElement);

            const detailsButton = document.createElement('button');
            detailsButton.textContent = 'Подробнее';
            detailsButton.classList.add('details-btn');
            detailsButton.addEventListener('click', async function() {
                try {
                    const clubDetails = await getClubById(club.id);
                    if (clubDetails) {
                        localStorage.setItem('clubDetails', JSON.stringify(clubDetails));
                        window.location.href = 'clubDetailsPage.html';
                    } else {
                        console.error('Детали клуба не найдены.');
                    }
                } catch (error) {
                    console.error('Ошибка при получении деталей клуба:', error);
                }
            });

            clubCard.appendChild(detailsButton);

            resultContainer.appendChild(clubCard);
        });
    }
}

function displayPagination(result, rowPerPage) {
    const paginationEl = document.querySelector('.pagination');
    const pagesCount = Math.ceil(result.length / rowPerPage);
    const ulEl = document.createElement("ul");
    ulEl.classList.add('pagination__list');

    paginationEl.innerHTML = "";

    for (let i = 0; i < pagesCount; i++) {
        const liEl = displayPaginationBtn(i + 1);
        ulEl.appendChild(liEl);
    }
    paginationEl.appendChild(ulEl);
}

function displayPaginationBtn(page) {
    const liEl = document.createElement("li");
    liEl.classList.add('pagination__item');
    liEl.innerText = page;

    if (currentPage === page) liEl.classList.add('pagination__item--active');

    liEl.addEventListener('click', async () => {
        currentPage = page;
        await displayResult(currentResult, rows, currentPage);

        let currentItemLi = document.querySelector('li.pagination__item--active');
        currentItemLi.classList.remove('pagination__item--active');

        liEl.classList.add('pagination__item--active');
    });

    return liEl;
}

displayResult(result, rows, currentPage);
displayPagination(currentResult, rows);

