package com.example.application.configurations;

import com.example.application.filters.AuthenticationFilter;
import com.example.application.filters.AuthorizationFilter;
import com.example.application.services.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfigurations {
    private final UserService service;
    private final PasswordEncoder encoder;
    private final AuthenticationConfiguration authenticationConfiguration;

    public SecurityConfigurations(UserService service, PasswordEncoder encoder, AuthenticationConfiguration authenticationConfiguration) {
        this.service = service;
        this.encoder = encoder;
        this.authenticationConfiguration = authenticationConfiguration;
    }

    @Bean
    public SecurityFilterChain chain(HttpSecurity security) throws Exception {
        AuthenticationFilter filter = new AuthenticationFilter(authenticationManager(authenticationConfiguration));
        filter.setFilterProcessesUrl("/login");

        security.authorizeHttpRequests(
                        (authorize) -> authorize.anyRequest().permitAll()
                )
                .csrf(AbstractHttpConfigurer::disable)
                .cors(
                        (cors) -> {
                            CorsConfiguration configuration = new CorsConfiguration();

                            configuration.addAllowedHeader("*");

                            configuration.addAllowedMethod("GET");
                            configuration.addAllowedMethod("POST");
                            configuration.addAllowedMethod("PUT");
                            configuration.addAllowedMethod("PATCH");
                            configuration.addAllowedMethod("DELETE");

                            configuration.addAllowedOrigin("http://localhost:63342/");
                            configuration.addAllowedOrigin("http://localhost:63343/");
                            configuration.addAllowedOrigin("http://localhost:63344/");
                            configuration.addAllowedOrigin("http://127.0.0.1:5500/");

                            configuration.setExposedHeaders(Arrays.asList("access_token", "refresh_token"));

                            configuration.setAllowCredentials(true);

                            UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
                            source.registerCorsConfiguration("/**", configuration);

                            cors.configurationSource(source);
                        }
                )
                .sessionManagement(
                        (sessions) -> sessions.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                );

        security.addFilter(filter);
        security.addFilterBefore(new AuthorizationFilter(), UsernamePasswordAuthenticationFilter.class);

        return security.build();
    }

    @Bean
    public DaoAuthenticationProvider provider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();

        provider.setUserDetailsService(service);
        provider.setPasswordEncoder(encoder);

        return provider;
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration configuration) throws Exception {
        return configuration.getAuthenticationManager();
    }
}

