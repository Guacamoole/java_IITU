package com.example.application.configurations;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.zalando.logbook.HttpRequest;
import org.zalando.logbook.Logbook;

import java.util.function.Predicate;

@Configuration
public class LogbookConfiguration {

    @Bean
    public Logbook logbook() {
        Logbook logbook = Logbook.builder()
                .build();
        return logbook;
    }

    private Predicate<HttpRequest> requestTo(String s) {
        return null;
    }
}
