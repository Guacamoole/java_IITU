package com.example.application.controllers;

import com.example.application.enums.TestFreeEnum;
import com.example.application.models.children.TestFree;
import com.example.application.services.TestFreeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/free_test")
@RequiredArgsConstructor
public class TestFreeController {
    private final TestFreeService testFreeService;
    @GetMapping
    public List<String> getTestFree() {
        return Arrays.stream(TestFreeEnum.values())
                .map(TestFreeEnum::getTitle)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public TestFree getTestFreeByChildId(@PathVariable Long id) {
        return testFreeService.getByChildId(id);
    }

    @PostMapping("/{id}")
    public void passTestFree(@PathVariable Long id, @RequestBody Integer[] choiceIds) {
        System.out.println("entered CONTROLLER");
        testFreeService.create(choiceIds, id);
    }

    @DeleteMapping("/{id}")
    public void deleteTestFree(@PathVariable Long id) {
        testFreeService.deleteByChildId(id);
    }
}

