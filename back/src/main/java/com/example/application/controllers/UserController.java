package com.example.application.controllers;

import com.example.application.DTO.users.CreateUserDTO;
import com.example.application.DTO.users.GetUserDTO;
import com.example.application.DTO.users.UpdateUserDTO;
import com.example.application.models.User;
import com.example.application.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService service;

    public UserController(UserService service) {
        this.service = service;
    }

    @PostMapping
    public void create(@RequestBody @Valid CreateUserDTO createUserDTO) {
        service.create(createUserDTO);
    }

    @GetMapping("/{username}")
    public ResponseEntity<GetUserDTO> get(@PathVariable String username) {
        try {
            User user = service.findByUsername(username);
            return ResponseEntity
                    .ok()
                    .body(GetUserDTO.map(user));
        }
        catch (IllegalArgumentException exception) {
            return ResponseEntity
                    .notFound()
                    .build();
        }
    }

    @PutMapping("/{username}")
    public void updateUserInfo(@PathVariable String username, @RequestBody @Valid UpdateUserDTO updateUserDTO) {
        service.updateUserInfo(username, updateUserDTO);
    }
}

