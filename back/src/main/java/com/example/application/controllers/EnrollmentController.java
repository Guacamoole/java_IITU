package com.example.application.controllers;

import com.example.application.DTO.EnrollmentRequestDTO;
import com.example.application.DTO.EnrollmentResponseDTO;
import com.example.application.models.children.Enrollment;
import com.example.application.models.clubs.Branch;
import com.example.application.services.EnrollmentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("enrollments")
public class EnrollmentController {

    private final EnrollmentService enrollmentService;

    public EnrollmentController(EnrollmentService enrollmentService) {
        this.enrollmentService = enrollmentService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<Enrollment>> getAllEnrollments() {
        List<Enrollment> enrollments = enrollmentService.getAll();
        return new ResponseEntity<>(enrollments, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public List<Branch> getAllClubsByChildId(@PathVariable Long id){
       return enrollmentService.getClubsByChildId(id);
    }

    @PostMapping
    public ResponseEntity<?> enrollChild(@RequestBody EnrollmentRequestDTO request) {
        try {
            Long enrollmentId = enrollmentService.enrollChildToClub(request.getChildId(), request.getClubId());
            EnrollmentResponseDTO response = new EnrollmentResponseDTO(enrollmentId);
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Произошла ошибка при записи ребенка в клуб: " + e.getMessage());
        }
    }



}
