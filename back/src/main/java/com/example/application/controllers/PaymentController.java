package com.example.application.controllers;

import com.example.application.models.children.Payment;
import com.example.application.services.PaymentService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;

@RestController
@AllArgsConstructor
@RequestMapping("/payment")
public class PaymentController {
    private final PaymentService paymentService;

    @PostMapping("/process")
    public CompletableFuture<Payment> processPayment(
            @RequestParam String cardNumber,
            @RequestParam String csv,
            @RequestParam Long enrollmentId) {
        return paymentService.processPayment(cardNumber, csv, enrollmentId);
    }
}
