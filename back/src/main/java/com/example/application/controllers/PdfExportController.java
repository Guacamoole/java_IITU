package com.example.application.controllers;

import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpServletResponseWrapper;

import com.example.application.models.clubs.Branch;
import com.example.application.models.children.Child;
import com.example.application.services.ChildService;
import com.example.application.services.EnrollmentService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import java.io.IOException;
import java.util.List;

@RestController
@AllArgsConstructor
public class PdfExportController {

    private final EnrollmentService enrollmentService;
    private final ChildService childService;

    @GetMapping("/export-pdf/{id}")
    public void exportDataToPdf(@PathVariable Long id, HttpServletResponse response) throws IOException {
        List<Branch> clubs = enrollmentService.getClubsByChildId(id);
        Child child = childService.getChildById(id);

        PDDocument document = new PDDocument();
        PDPage page = new PDPage();
        document.addPage(page);

        try (PDPageContentStream contentStream = new PDPageContentStream(document, page)) {
            contentStream.setFont(PDType1Font.HELVETICA_BOLD, 12);
            contentStream.beginText();
            contentStream.newLineAtOffset(100, 700);
            contentStream.showText("Child Details: ");
            contentStream.newLineAtOffset(0, -15);
            contentStream.showText("Child Name: " + child.getFirstname());
            contentStream.newLineAtOffset(0, -15);
            contentStream.showText("Child Lastname: " + child.getLastname());
            contentStream.newLineAtOffset(0, -15);

            contentStream.setFont(PDType1Font.HELVETICA, 12);
            contentStream.showText("Clubs:");
            contentStream.newLineAtOffset(0, -15);
            for (Branch club : clubs) {
                contentStream.showText("- " + club.getName());
                contentStream.newLineAtOffset(0, -15);
                contentStream.showText("- " + club.getAddress());
                contentStream.newLineAtOffset(0, -15);
                contentStream.showText("- " + club.getSchedule());
                contentStream.newLineAtOffset(0, -15);
                contentStream.showText("- " + club.getPhoneNumber());
                contentStream.newLineAtOffset(0, -15);
            }

            contentStream.endText();
        }

        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=\"exported_data.pdf\"");

        HttpServletResponseWrapper responseWrapper = new HttpServletResponseWrapper(response);
        document.save(responseWrapper.getOutputStream());
        document.close();
    }

}
