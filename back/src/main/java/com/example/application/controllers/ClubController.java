package com.example.application.controllers;

import com.example.application.DTO.clubs.CreateBranchDTO;
import com.example.application.DTO.clubs.CreateClubDTO;
import com.example.application.DTO.clubs.GetClubDTO;
import com.example.application.services.clubs.ClubService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/clubs")
public class ClubController {
    private final ClubService clubService;

    public ClubController(ClubService clubService) {
        this.clubService = clubService;
    }

    @GetMapping
    public List<GetClubDTO> getAllBranches() {
        return clubService.getAllOrderByName();
    }

    @PostMapping()
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void addClub(@RequestBody CreateClubDTO createClubDTO) {
        clubService.add(createClubDTO);
    }

    @PostMapping("/{id}/addBranch")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void addBranch(@PathVariable String id, @RequestBody CreateBranchDTO createBranchDTO) {
        clubService.addBranch(id, createBranchDTO);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void deleteClub(@PathVariable Long id) {
        clubService.delete(id);
    }
}
