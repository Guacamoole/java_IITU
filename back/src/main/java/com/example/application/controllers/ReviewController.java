package com.example.application.controllers;

import com.example.application.DTO.ReviewDTO;
import com.example.application.services.ReviewService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("reviews")
public class ReviewController {

    private final ReviewService reviewService;

    public ReviewController(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    @PostMapping("/add")
    public ResponseEntity<?> addReview(@RequestBody ReviewDTO reviewDTO) {
        reviewService.addReview(reviewDTO);
        return ResponseEntity.ok().body("Отзыв успешно добавлен");
    }

    @GetMapping("/club/{branchId}")
    public ResponseEntity<List<ReviewDTO>> getReviewsByBranch(@PathVariable Long branchId) {
        List<ReviewDTO> reviews = reviewService.findReviewsByBranchId(branchId);
        return ResponseEntity.ok(reviews);
    }
}
