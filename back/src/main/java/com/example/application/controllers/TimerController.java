package com.example.application.controllers;

import com.example.application.services.TimerService;
import lombok.AllArgsConstructor;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;

@Controller
@AllArgsConstructor
public class TimerController {
    private final TimerService timerService;

    @MessageMapping("/startTimer")
    public void startTimerForUser(Principal principal){
        System.out.println("Invoke startTimerForUser");
        timerService.startTimer(principal.getName());
    }

    @GetMapping("/stopTimer")
    @ResponseBody
    public void stopTimerForUser(Principal principal) {
        System.out.println("Invoke stopTimerForUser");
        timerService.stopTimer(principal.getName());
    }
}

