package com.example.application.services.impl;

import com.example.application.models.children.Child;
import com.example.application.models.User;
import com.example.application.repositories.ChildRepository;
import com.example.application.services.ChildService;
import com.example.application.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class ChildServiceImpl implements ChildService {

    private final ChildRepository childRepository;
    private final UserService userService;


    @Override
    public void addChild(String username, Child createChildDTO) {
        User user = userService.findByUsername(username);

        Child child = new Child();
        child.setUserId(user.getId());
        child.setFirstname(createChildDTO.getFirstname());
        child.setLastname(createChildDTO.getLastname());
        child.setGender(createChildDTO.getGender());
        child.setBirthYear(createChildDTO.getBirthYear());

        childRepository.save(child);

        userService.updateChildren(username,child);
    }

    @Override
    public List<Child> getAllChildren() {
        return childRepository.findAll();
    }

    @Override
    public Child getChildById(Long id) {
        return childRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Child with id " + id + " not found."));
    }

    @Override
    public void deleteChild(Long id) {
        if (!childRepository.existsById(id)) {
            throw new IllegalArgumentException("Child with id " + id + " does not exist.");
        }
        childRepository.deleteById(id);
    }

    @Override
    public List<Child> findAllChildrenByUserId(Long id) {
        return childRepository.findAllByUserId(id);
    }
}

