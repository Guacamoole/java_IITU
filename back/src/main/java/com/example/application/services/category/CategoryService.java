package com.example.application.services.category;

import com.example.application.DTO.categories.CreateCategoryDTO;
import com.example.application.models.clubs.Category;

import java.util.List;

public interface CategoryService {
    List<Category> getAllCategories();
    Category getCategoryById(Long id);
    void addCategory(CreateCategoryDTO categoryDTO);

    void editCategory(Long id, Category updatedCategory);

    void deleteCategory(Long id);
}

