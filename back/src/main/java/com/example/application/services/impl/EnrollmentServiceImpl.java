package com.example.application.services.impl;

import com.example.application.models.clubs.Branch;
import com.example.application.models.children.Child;
import com.example.application.models.children.Enrollment;
import com.example.application.repositories.ChildRepository;
import com.example.application.repositories.EnrollmentRepository;
import com.example.application.services.EnrollmentService;
import com.example.application.services.clubs.BranchService;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class EnrollmentServiceImpl implements EnrollmentService {

    private final ChildRepository childRepository;
    private final EnrollmentRepository enrollmentRepository;
    private final BranchService branchService;

    @Override
    public List<Enrollment> getAll() {
        return enrollmentRepository.findAll();
    }

    @Override
    public List<Branch> getClubsByChildId(Long childId) {
        return enrollmentRepository.findAllByChildId(childId).stream()
                .map(Enrollment::getBranch)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public Long enrollChildToClub(Long childId, Long branchId) {

        if (enrollmentRepository.existsByChildIdAndBranchId(childId, branchId)) {
            throw new RuntimeException("Этот ребенок уже записан в данный клуб");
        }

        Child child = childRepository.findById(childId)
                .orElseThrow(() -> new RuntimeException("Ребенок с ID " + childId + " не найден"));

        Branch branch = branchService.getBranchById(branchId);

        if (branch == null) {
            throw new UsernameNotFoundException("Клуб не найден");
        }

        Enrollment enrollment = new Enrollment();
        enrollment.setChild(child);
        enrollment.setBranch(branch);

        enrollmentRepository.save(enrollment);
        return enrollment.getId();
    }
}
