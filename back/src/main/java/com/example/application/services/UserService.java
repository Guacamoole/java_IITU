package com.example.application.services;


import com.example.application.models.User;
import com.example.application.DTO.users.CreateUserDTO;
import com.example.application.DTO.users.UpdateUserDTO;
import com.example.application.models.children.Child;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {
    User findByUsername(String username);
    List<Child> getAllChildrenByUserId(Long userId);
    void updateChildren(String username, Child child);
    void create(CreateUserDTO createUserDTO);
    void updateUserInfo(String username, UpdateUserDTO updateUserDTO);

    User getUserById(Long userId);
}
