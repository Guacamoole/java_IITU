package com.example.application.services.clubs.impl;

import com.example.application.models.clubs.Branch;
import com.example.application.models.clubs.Club;
import com.example.application.models.clubs.ClubBranch;
import com.example.application.repositories.ClubBranchRepository;
import com.example.application.services.clubs.ClubBranchService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClubBranchServiceImpl implements ClubBranchService {
    private final ClubBranchRepository clubBranchRepository;

    public ClubBranchServiceImpl(ClubBranchRepository clubBranchRepository) {
        this.clubBranchRepository = clubBranchRepository;
    }

    @Override
    public void add(Club club, Branch branch) {
        ClubBranch clubBranch = ClubBranch
                                        .builder()
                                        .club(club)
                                        .branch(branch)
                                        .build();

        clubBranchRepository.save(clubBranch);
    }

    @Override
    public void delete(Long id) {
        clubBranchRepository.deleteById(id);
    }

    @Override
    public List<ClubBranch> findByBranchId(Long branchId) {
        return clubBranchRepository.findAllByBranchId(branchId);
    }

    @Override
    public List<ClubBranch> findAllByClubId(Long clubId) {
        return clubBranchRepository.findAllByClubId(clubId);
    }

    @Override
    public List<ClubBranch> getAllByCategoryId(Long categoryId) {
        return clubBranchRepository.findAllByClubCategoryId(categoryId);
    }

    @Override
    public List<ClubBranch> getAllBySubcategoryId(Long subcategoryId) {
        return clubBranchRepository.findAllByClubSubcategoryId(subcategoryId);
    }

}
