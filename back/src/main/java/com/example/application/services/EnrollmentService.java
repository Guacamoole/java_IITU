package com.example.application.services;

import com.example.application.models.children.Enrollment;
import com.example.application.models.clubs.Branch;

import java.util.List;

public interface EnrollmentService {
    List<Enrollment> getAll();

    List<Branch> getClubsByChildId(Long childId);
//    void enrollChildToClub(Long childId, Long clubId);
    Long enrollChildToClub(Long childId, Long branchId);
}
