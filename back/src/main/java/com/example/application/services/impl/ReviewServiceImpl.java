package com.example.application.services.impl;

import com.example.application.DTO.ReviewDTO;
import com.example.application.models.clubs.Branch;
import com.example.application.models.clubs.Review;
import com.example.application.models.User;
import com.example.application.repositories.ReviewRepository;
import com.example.application.services.ReviewService;
import com.example.application.services.UserService;
import com.example.application.services.clubs.BranchService;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReviewServiceImpl implements ReviewService {

    private final BranchService branchService;
    private final ReviewRepository reviewRepository;
    private final UserService userService;

    public ReviewServiceImpl(BranchService branchService, ReviewRepository reviewRepository, UserService userService) {
        this.branchService = branchService;
        this.reviewRepository = reviewRepository;
        this.userService = userService;
    }

    @Override
    public void addReview(ReviewDTO reviewDTO) {

        User user = userService.findByUsername(reviewDTO.getUsername());

        Branch branch = branchService.getBranchById(reviewDTO.getClubId());

        long reviewsCount = reviewRepository.countByUserAndBranch(user, branch);

        if (reviewsCount >= 5) {
            throw new RuntimeException("Нельзя добавить более 5 отзывов на один клуб");
        }

        Review review = new Review();
        review.setUser(user);
        review.setBranch(branch);
        review.setComment(reviewDTO.getComment());
        review.setRating(reviewDTO.getRating());

        reviewRepository.save(review);
        getAverageRatingForClub(branch.getId());

    }

    @Override
    public List<ReviewDTO> findReviewsByBranchId(Long branchId) {
        List<Review> reviews = reviewRepository.findByBranchId(branchId);
        return reviews.stream().map(this::convertToDTO).collect(Collectors.toList());
    }

    private ReviewDTO convertToDTO(Review review) {
        return new ReviewDTO(review.getUser().getUsername(), review.getBranch().getId(), review.getComment(), review.getRating(), review.getCreatedAt().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm")));
    }

    @Override
    public Double getAverageRatingForClub(Long branchId) {
        Branch branch = branchService.getBranchById(branchId);

        if (branch != null) {
            List<Review> reviews = reviewRepository.findByBranchId(branchId);

            double sum = reviews.stream().mapToDouble(Review::getRating).sum();
            double averageRating = reviews.isEmpty() ? 0 : sum / reviews.size();

            int decimalPlaces = 1;
            double roundedRating = Math.round(averageRating * Math.pow(10, decimalPlaces)) / Math.pow(10, decimalPlaces);

            branch.setRating(roundedRating);

            return branchService.save(branch).getRating();
        }
        else {
            return null;
        }
    }
}