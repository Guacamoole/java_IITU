package com.example.application.services.clubs;

import com.example.application.DTO.clubs.CreateBranchDTO;
import com.example.application.DTO.clubs.UpdateBranchDTO;
import com.example.application.models.clubs.Branch;

import java.util.List;

public interface BranchService {
    List<Branch> getAllBranches();
    Branch getBranchById(Long id);
    void add(CreateBranchDTO createBranchDTO);
    void delete(Long id);
    void deactivate(Long id);
    void edit(Long branchId, UpdateBranchDTO updateBranchDTO);
    Branch save(Branch branch);
    List<Branch> sortByRating(List<Branch> unsortedList);

    List<Branch> filterBranchesByCategory(String category);

    List<Branch> filterBranchesBySubcategory(String subcategory);

    List<Branch> searchBranchesByName(String name);

    List<Branch> getAllBranchesSortedByRating();
    List<Branch> getAllByOrderByNameAsc();
    List<Branch> getAllByActiveOrderByNameAsc(boolean active);
}
