package com.example.application.services.category;

import com.example.application.DTO.categories.CreateSubCategoryDTO;
import com.example.application.models.clubs.Subcategory;

import java.util.List;

public interface SubcategoryService {
    List<Subcategory> getAllSubcategories();
    Subcategory getSubcategoryById(Long id);
    void addSubcategory(CreateSubCategoryDTO subcategoryDTO);

    void editSubcategory(Long id, Subcategory updatedSubcategory);

    void deleteSubcategory(Long id);

    List<Subcategory> getSubcategoriesByCategoryId(Long categoryId);
}
