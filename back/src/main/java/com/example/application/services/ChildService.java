package com.example.application.services;

import com.example.application.models.children.Child;

import java.util.List;

public interface ChildService {
    void addChild(String username, Child childDTO);
    List<Child> getAllChildren();
    Child getChildById(Long id);
    void deleteChild(Long id);
    List<Child> findAllChildrenByUserId(Long id);
}