package com.example.application.services.impl;

import com.example.application.enums.TestFreeEnum;
import com.example.application.models.children.Child;
import com.example.application.models.children.TestFree;
import com.example.application.repositories.TestFreeRepository;
import com.example.application.services.ChildService;
import com.example.application.services.TestFreeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class TestFreeServiceImpl implements TestFreeService {
    private final TestFreeRepository testFreeRepository;
    private final ChildService childService;
    @Override
    public Optional<TestFree> getById(Long id) {
        return testFreeRepository.findById(id);
    }

    @Override
    public TestFree getByChildId(Long childId) {
        return testFreeRepository.findByChildId(childId);
    }

    @Override
    public List<TestFree> getAllTestFrees() {
        return testFreeRepository.findAll();
    }

    @Override
    public void create(Integer[] choiceIds, Long childId) {
        TestFree testFree = testFreeRepository.findByChildId(childId);
        if (testFree == null || testFree.getResults().isEmpty()){
            Map<Integer, Integer> countMap = new HashMap<>();
            for (Integer num : choiceIds) {
                countMap.put(num, countMap.getOrDefault(num, 0) + 1);
            }

            List<Integer> chosenTwiceIds = new ArrayList<>();
            for (Map.Entry<Integer, Integer> entry : countMap.entrySet()) {
                if (entry.getValue() > 1) {
                    chosenTwiceIds.add(entry.getKey());
                    System.out.println("Число " + entry.getKey() + " встречается " + entry.getValue() + " раз(а)");
                }
            }
            List<String> results = new ArrayList<>();
            if(!chosenTwiceIds.isEmpty()){
                for(Integer chosenId: chosenTwiceIds){
                    results.add(TestFreeEnum.valueOf(chosenId).getTitle());
                }
            }

            if(!results.isEmpty()){
                testFree = new TestFree();

                testFree.setTitle("Бесплатный тест");
                testFree.setResults(results);
                testFree.setChildId(childId);

                Child child = childService.getChildById(childId);
                child.setTestFree(testFree);

                testFreeRepository.save(testFree);
            }
        }
    }
    @Override
    public void deleteByChildId(Long id) {
        testFreeRepository.deleteByChildId(id);
    }
}

