package com.example.application.services.impl;

import com.example.application.DTO.users.CreateUserDTO;
import com.example.application.DTO.users.UpdateUserDTO;
import com.example.application.exceptions.DuplicateException;
import com.example.application.exceptions.MaxDataException;
import com.example.application.models.Authority;
import com.example.application.models.children.Child;
import com.example.application.models.User;
import com.example.application.repositories.UserRepository;
import com.example.application.services.AuthorityService;
import com.example.application.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final AuthorityService authorityService;
    private final PasswordEncoder passwordEncoder;


    @Override
    public void create(CreateUserDTO createUserDTO) {
        List<User> existingUsers = userRepository.findAll();
        for(User user : existingUsers){
            if(user.getUsername().equals(createUserDTO.getUsername())){
                throw new DuplicateException("User already exists", 500);
            }
        }
        List<Authority> authorities = authorityService.findAllByAuthority("ROLE_USER");

        User user = User.builder()
                .username(createUserDTO.getUsername())
                .firstname(createUserDTO.getFirstname())
                .lastname(createUserDTO.getLastname())
                .phone(createUserDTO.getPhone())
                .authorities(authorities)
                .password(passwordEncoder.encode(createUserDTO.getPassword()))
                .build();
        userRepository.save(user);
    }

    @Override
    public User findByUsername(String username) {
        Optional<User> user = userRepository.findByUsername(username);
        if (user.isEmpty()) {
            throw new UsernameNotFoundException("User with username %s doesn't exists!".formatted(username));
        }
        return user.get();
    }

    @Override
    public List<Child> getAllChildrenByUserId(Long userId) {
        return userRepository.findAllById(userId);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUsername(username);
        if (user.isEmpty()) {
            throw new UsernameNotFoundException("User with username %s doesn't exists!".formatted(username));
        }
        return user.get();
    }

    @Override
    public void updateUserInfo(String username, UpdateUserDTO updateUserDTO) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User with username %s doesn't exist!".formatted(username)));
        String newFirstname = updateUserDTO.getFirstname();
        if(!newFirstname.isEmpty() || !newFirstname.isBlank()){
            user.setFirstname(newFirstname);
        }

        String newLastname = updateUserDTO.getLastname();
        if(!newLastname.isEmpty() || !newLastname.isBlank()){
            user.setLastname(newLastname);
        }
        String newPhone = updateUserDTO.getPhone();
        if(!newPhone.isEmpty() || !newPhone.isBlank()){
            user.setPhone(newPhone);
        }
        user.setPhotoUrl(updateUserDTO.getPhotoUrl());

        userRepository.save(user);
    }

    @Override
    public void updateChildren(String username, Child child) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User with username %s doesn't exist!".formatted(username)));
        List<Child> childList = user.getChildren();
        if(childList == null || childList.isEmpty()){
            childList = new ArrayList<>();
        }
        if(childList.size() >= 10){
            throw new MaxDataException("Максимальное количество детей достигнуто");
        }

        childList.add(child);
        user.setChildren(childList);
        userRepository.save(user);
    }

    @Override
    public User getUserById(Long userId) {
        return userRepository.findById(userId)
                .orElseThrow(() -> new NoSuchElementException("User not found with ID: " + userId));
    }
}

