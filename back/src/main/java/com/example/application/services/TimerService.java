package com.example.application.services;

public interface TimerService {
    void startTimer(String name);

    void stopTimer(String name);
}
