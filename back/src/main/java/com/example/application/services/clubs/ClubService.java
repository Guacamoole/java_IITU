package com.example.application.services.clubs;

import com.example.application.DTO.clubs.CreateBranchDTO;
import com.example.application.DTO.clubs.CreateClubDTO;
import com.example.application.DTO.clubs.GetClubDTO;
import com.example.application.models.clubs.Club;

import java.util.List;

public interface ClubService {
    List<Club> getAllClubs();
    List<GetClubDTO> getAllOrderByName();
    Club getClubById(Long id);
    void add(CreateClubDTO createClubDTO);
    void delete(Long id);
    void addBranch(String clubId, CreateBranchDTO createBranchDTO);
}
