package com.example.application.services;

import com.example.application.models.children.Payment;
import org.springframework.scheduling.annotation.Async;

import java.util.concurrent.CompletableFuture;

public interface PaymentService {
    @Async
    CompletableFuture<Payment> processPayment(String cardNumber, String csv, Long enrollmentId);
}
