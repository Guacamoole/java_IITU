package com.example.application.services.category.impl;

import com.example.application.DTO.categories.CreateSubCategoryDTO;
import com.example.application.models.clubs.Subcategory;
import com.example.application.repositories.SubcategoryRepository;
import com.example.application.services.category.CategoryService;
import com.example.application.services.category.SubcategoryService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class SubcategoryServiceImpl implements SubcategoryService {

    private final SubcategoryRepository subcategoryRepository;
    private final CategoryService categoryService;

    public SubcategoryServiceImpl(SubcategoryRepository subcategoryRepository, CategoryService categoryService) {
        this.subcategoryRepository = subcategoryRepository;
        this.categoryService = categoryService;
    }

    @Override
    public List<Subcategory> getAllSubcategories() {
        return subcategoryRepository.findAll();
    }

    @Override
    public Subcategory getSubcategoryById(Long id) {
        try {
            return subcategoryRepository.findById(id)
                    .orElseThrow(() -> new NoSuchElementException("Подкатегория с ID " + id + " не найдена"));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public void addSubcategory(CreateSubCategoryDTO subcategoryDTO) {
        Subcategory subcategory = Subcategory
                .builder()
                .name(subcategoryDTO.getName())
                .category(categoryService.getCategoryById(subcategoryDTO.getCategoryId()))
                .build();

        subcategoryRepository.save(subcategory);
    }

    @Override
    public void editSubcategory(Long id, Subcategory updatedSubcategory) {
        Subcategory existingSubCategory = subcategoryRepository.findById(id).orElseThrow();

        if (updatedSubcategory.getName() != null) {
            existingSubCategory.setName(updatedSubcategory.getName());
        }

        subcategoryRepository.save(existingSubCategory);
    }


    @Override
    public void deleteSubcategory(Long id) {
        subcategoryRepository.deleteById(id);
    }

    @Override
    public List<Subcategory> getSubcategoriesByCategoryId(Long categoryId) {
        return subcategoryRepository.findByCategoryId(categoryId);
    }
}
