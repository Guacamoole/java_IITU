package com.example.application.services.impl;

import com.example.application.services.AuthorityService;
import com.example.application.models.Authority;
import com.example.application.repositories.AuthorityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AuthorityServiceImpl implements AuthorityService {
    private final AuthorityRepository repository;
    @Override
    public List<Authority> findAllByAuthority(String authority) {
        return repository.findAllByAuthority(authority);
    }
}
