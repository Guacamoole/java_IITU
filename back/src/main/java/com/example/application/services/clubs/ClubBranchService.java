package com.example.application.services.clubs;

import com.example.application.models.clubs.Branch;
import com.example.application.models.clubs.Club;
import com.example.application.models.clubs.ClubBranch;

import java.util.List;

public interface ClubBranchService {
    void add(Club club, Branch branch);
    void delete(Long id);
    List<ClubBranch> findByBranchId(Long branchId);
    List<ClubBranch> findAllByClubId(Long clubId);
    List<ClubBranch> getAllByCategoryId(Long categoryId);
    List<ClubBranch> getAllBySubcategoryId(Long subcategoryId);

}
