package com.example.application.services.impl;

import com.example.application.services.TimerService;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicReference;

@Service
@RequiredArgsConstructor
public class TimerServiceImpl implements TimerService {

    private final SimpMessagingTemplate template;
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(5);
    private final Map<String, ScheduledFuture<?>> userTimerFuture = new ConcurrentHashMap<>();

    @Override
    public void startTimer(String name) {
        if (userTimerFuture.containsKey(name)) {
            return;
        }

        final long[] duration = {Duration.ofMinutes(5).getSeconds()};;
        final AtomicReference<ScheduledFuture<?>> futureRef = new AtomicReference<>();

        ScheduledFuture<?> future = scheduler.scheduleAtFixedRate(() -> {
            if (duration[0] < 0) {
                futureRef.get().cancel(true);
                userTimerFuture.remove(name);
                return;
            }

            String timeString = LocalTime.MIDNIGHT.plusSeconds(duration[0]).format(DateTimeFormatter.ofPattern("HH:mm:ss"));
            template.convertAndSendToUser(name, "/queue/test", timeString);

            duration[0]--;
        }, 0, 1, TimeUnit.SECONDS);

        futureRef.set(future);
        userTimerFuture.put(name, future);
    }

    @Override
    public void stopTimer(String name) {
        if (userTimerFuture.containsKey(name)) {
            ScheduledFuture<?> future = userTimerFuture.get(name);
            if (future != null) {
                future.cancel(true);
                userTimerFuture.remove(name);
            }
        }
    }
}
