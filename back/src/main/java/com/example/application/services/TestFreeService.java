package com.example.application.services;

import com.example.application.models.children.TestFree;

import java.util.List;
import java.util.Optional;

public interface TestFreeService {
    Optional<TestFree> getById(Long id);
    TestFree getByChildId(Long childId);
    List<TestFree> getAllTestFrees();

    void create(Integer[] choiceIds, Long childId);
    void deleteByChildId(Long id);
}
