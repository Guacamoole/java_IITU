package com.example.application.services.category.impl;

import com.example.application.DTO.categories.CreateCategoryDTO;
import com.example.application.models.clubs.Category;
import com.example.application.repositories.CategoryRepository;
import com.example.application.services.category.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> getAllCategories() {
        return categoryRepository.findAll();
    }

    @Override
    public Category getCategoryById(Long id) {
        try {
            return categoryRepository.findById(id)
                    .orElseThrow(() -> new NoSuchElementException("Категория с ID " + id + " не найдена"));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void addCategory(CreateCategoryDTO categoryDTO) {
        Category category = Category
                .builder()
                .name(categoryDTO.getName())
                .build();
        
        categoryRepository.save(category);
    }

    @Override
    public void editCategory(Long id, Category updatedCategory) {
        Category existingCategory = categoryRepository.findById(id).orElseThrow();

        if (updatedCategory.getName() != null) {
            existingCategory.setName(updatedCategory.getName());
        }

        categoryRepository.save(existingCategory);
    }

    @Override
    public void deleteCategory(Long id) {
        categoryRepository.deleteById(id);
    }
}
