package com.example.application.services.impl;

import com.example.application.models.children.Enrollment;
import com.example.application.models.children.Payment;
import com.example.application.repositories.EnrollmentRepository;
import com.example.application.repositories.PaymentRepository;
import com.example.application.services.PaymentService;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Service
@AllArgsConstructor
public class PaymentServiceImpl implements PaymentService {
    private final PaymentRepository paymentRepository;

    private final EnrollmentRepository enrollmentRepository;

    @Override
    @Async
    public CompletableFuture<Payment> processPayment(String cardNumber, String csv, Long enrollmentId) {
        try {
            Thread.sleep(30000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        Payment payment = new Payment();
        payment.setCardNumber(cardNumber);
        payment.setCsv(csv);
        payment.setAmount(15000);
        payment.setStatus("Completed");

        Enrollment enrollment = enrollmentRepository.findById(enrollmentId)
                .orElseThrow(() -> new EntityNotFoundException("Enrollment not found"));
        payment.setEnrollment(enrollment);

        enrollment.setPaid(true);
        enrollmentRepository.save(enrollment);

        Payment savedPayment = paymentRepository.save(payment);
        return CompletableFuture.completedFuture(savedPayment);
    }
}

