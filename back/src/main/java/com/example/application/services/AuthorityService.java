package com.example.application.services;

import com.example.application.models.Authority;

import java.util.List;

public interface AuthorityService {
    List<Authority> findAllByAuthority(String authority);
}
