package com.example.application.DTO.users;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreateClubDTO {
    private String name;
    private Long categoryID;
    private Long subcategoryID;
    private String address;
    private String phoneNumber;
    private String instagram;
    private String services;
    private String teachers;
    private String schedule;
    private String image;
}
