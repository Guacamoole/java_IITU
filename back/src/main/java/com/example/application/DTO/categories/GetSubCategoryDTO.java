package com.example.application.DTO.categories;

import com.example.application.models.clubs.Subcategory;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GetSubCategoryDTO {
    private Long id;

    private String name;

    public static GetSubCategoryDTO map(Subcategory subCategory) {
        return new GetSubCategoryDTO(subCategory.getId(), subCategory.getName());
    }

}
