package com.example.application.DTO.users;

import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UpdateUserDTO {
    private String firstname;
    private String lastname;
    private String phone;
    private String photoUrl;
}

