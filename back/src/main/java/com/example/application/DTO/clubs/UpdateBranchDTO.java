package com.example.application.DTO.clubs;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UpdateBranchDTO {
    private String name;
    private String address;
    private String phoneNumber;
    private String service;
    private boolean mainBranch;
    private String teachers;
    private String schedule;
    private String instagram;
}
