package com.example.application.DTO.clubs;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreateClubDTO {

    @NotBlank
    private String name;

    @NotNull
    private Long categoryID;

    private Long subcategoryID;

    private List<CreateBranchDTO> branches;

}
