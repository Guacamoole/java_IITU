package com.example.application.enums;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
public enum TestFreeEnum {
    BASKETBALL(1, "Баскетбол"),
    BICYCLING(2, "Велосипедный спорт"),
    BORBA(3, "Борьба"),
    BOXING(4, "Бокс"),
    CHESS(5, "Шахматы"),
    DOUBLE_TENNIS(6, "Парный теннис"),
    EDINOBORSTVO(7, "Единоборство"),
    FOOTBALL(8, "Футбол"),
    GROUP_GYMNASTIC(9, "Групповая гимнастика"),
    GYMNASTIC(10, "Гимнастика"),
    HANDBALL(11, "Гандбол"),
    HOCKEY(12, "Хоккей"),
    SKATING(13, "Фигурное катание"),
    SKIING(14, "Лыжный спорт"),
    SPORT_DANCE(15, "Спортивные танцы"),
    SWIMMING(16, "Плавание"),
    SYNHRONIZED_SWIMMING(17, "Синхронное плавание"),
    TENNIS_INDIVIDUAL(18, "Теннис"),
    VOLEYBALL(19, "Волейбол"),
    WATER_POLO(20, "Водное поло");

    private final Integer id;
    private final String title;

    TestFreeEnum(Integer id, String title) {
        this.id = id;
        this.title = title;
    }

    private static final Map<Integer, TestFreeEnum> BY_ID = new HashMap<>();

    static {
        for (TestFreeEnum e : values()) {
            BY_ID.put(e.id, e);
        }
    }

    public static TestFreeEnum valueOf(int id) {
        return BY_ID.get(id);
    }
}
