package com.example.application.utils;

import com.example.application.models.clubs.Branch;
import com.example.application.DTO.clubs.CreateBranchDTO;


public class ClubMapper {
    public static Branch mapClubBranch(CreateBranchDTO dto) {
        return new Branch(dto.getName(), dto.getAddress(), dto.getPhoneNumber(), dto.getService(),
                dto.isMainBranch(), dto.getTeachers(), dto.getSchedule(), dto.getInstagram());
    }
}
