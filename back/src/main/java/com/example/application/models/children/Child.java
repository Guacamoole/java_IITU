package com.example.application.models.children;

import com.example.application.models.BaseEntity;
import com.example.application.models.children.TestFree;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.*;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "children")
public class Child extends BaseEntity {
    @Column(name = "user_id")
    private Long userId;

    @NotBlank
    private String firstname;

    private String lastname;
    private String gender;

    private int birthYear;

    @OneToOne
    private TestFree testFree;
}