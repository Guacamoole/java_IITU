package com.example.application.models.clubs;

import com.example.application.models.BaseEntity;
import com.example.application.models.User;
import com.example.application.models.clubs.Branch;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "reviews")
public class Review extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "branch_id")
    private Branch branch;

    private String comment;

    private Integer rating;

    @Column(nullable = false)
    private LocalDateTime createdAt = LocalDateTime.now();
}

