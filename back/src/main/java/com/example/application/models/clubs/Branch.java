package com.example.application.models.clubs;

import com.example.application.models.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.*;

import java.util.List;
import java.util.Set;

@Builder
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "branches")
public class Branch extends BaseEntity {
    private String name;
    private String address;
    private String phoneNumber;
    private String service;
    private boolean mainBranch;
    private String teachers;
    private String schedule;
    private boolean active = true;
    private String instagram;
    private Double rating = 0.0;

    @JsonIgnore
    @OneToMany(mappedBy = "branch")
    private List<ClubBranch> clubBranches;

    @JsonIgnore
    @OneToMany(mappedBy = "branch")
    private Set<Review> reviews;

    public Branch(String name, String address, String phoneNumber, String service, boolean mainBranch, String teachers, String schedule, String instagram) {
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.service = service;
        this.mainBranch = mainBranch;
        this.teachers = teachers;
        this.schedule = schedule;
        this.instagram = instagram;
    }

}
