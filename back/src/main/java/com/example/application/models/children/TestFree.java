package com.example.application.models.children;

import com.example.application.models.BaseEntity;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Getter @Setter
@NoArgsConstructor
@Table(name = "test_free")
public class TestFree extends BaseEntity {
    private String title;

    @ElementCollection
    @CollectionTable(name = "test_free_results", joinColumns = @JoinColumn(name = "test_free_id"))
    @Column(name = "results")
    private List<String> results;

    @Column(name = "child_id")
    private Long childId;
}
