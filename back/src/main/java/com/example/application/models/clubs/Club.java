package com.example.application.models.clubs;

import com.example.application.models.BaseEntity;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Builder
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "clubs")
public class Club extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    private Category category;

    @ManyToOne(fetch = FetchType.LAZY)
    private Subcategory subcategory;

    private String name;

    @OneToMany(mappedBy = "club")
    private List<ClubBranch> clubBranches;
}