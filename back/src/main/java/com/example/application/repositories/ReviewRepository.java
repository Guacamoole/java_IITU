package com.example.application.repositories;

import com.example.application.models.clubs.Branch;
import com.example.application.models.clubs.Review;
import com.example.application.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ReviewRepository extends JpaRepository<Review, Long> {
    List<Review> findByBranchId(Long clubId);

    long countByUserAndBranch(User user, Branch branch);
}
