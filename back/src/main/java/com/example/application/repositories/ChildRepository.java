package com.example.application.repositories;

import com.example.application.models.children.Child;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ChildRepository extends JpaRepository<Child, Long> {
    Optional<Child> findByUserId (Long userId);
    List<Child> findAllByUserId(Long id);
}
