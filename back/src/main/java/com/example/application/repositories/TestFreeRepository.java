package com.example.application.repositories;

import com.example.application.models.children.TestFree;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TestFreeRepository extends JpaRepository<TestFree, Long> {
    TestFree findByChildId(Long id);

    void deleteByChildId(Long id);
}
