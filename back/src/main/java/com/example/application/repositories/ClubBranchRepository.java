package com.example.application.repositories;


import com.example.application.models.clubs.ClubBranch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClubBranchRepository extends JpaRepository<ClubBranch, Long> {
    List<ClubBranch> findAllByBranchId(Long branchId);
    List<ClubBranch> findAllByClubId(Long clubId);
    List<ClubBranch> findAllByClubCategoryId(Long categoryId);
    List<ClubBranch> findAllByClubSubcategoryId(Long subcategoryId);
}
